import * as _ from 'lodash';
import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';

import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';

import {Level, LoggerModule, LoggerService} from '@ngx-toolkit/logger';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule} from '@angular/common/http';

import {NgxPwaInstallModule} from 'ngx-pwa-install';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {CustomMaterialModule} from './custom-material.module';

import {HomeComponent} from './home/home.component';
import {GameComponent} from './game/game.component';
import {ProfileComponent} from './profile/profile.component';
import {AuthModule, ConfigResult, OidcConfigService, OidcSecurityService, OpenIdConfiguration} from 'angular-auth-oidc-client';
import {RouterModule} from '@angular/router';


const oidcConfiguration = 'assets/auth.clientConfiguration.json';
// if your config is on server side
// const oidc_configuration = ${window.location.origin}/api/ClientAppSettings

export function loadConfig(oidcConfigService: OidcConfigService) {
  return () => oidcConfigService.load(oidcConfiguration);
}

const LOG_LEVEL: Level = environment.production ? Level.ERROR : Level.DEBUG;
// isDevMode() ? Level.DEBUG : Level.ERROR;

@NgModule({
  declarations: [AppComponent, HomeComponent, GameComponent, ProfileComponent],
  imports: [
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    AppRoutingModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    CustomMaterialModule,
    NgxPwaInstallModule.forRoot(),
    AuthModule.forRoot(),
    LoggerModule.forRoot(LOG_LEVEL),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    RouterModule,
  ],
  providers: [
    OidcConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadConfig,
      deps: [OidcConfigService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private logger: LoggerService,
              private library: FaIconLibrary,
              private oidcSecurityService: OidcSecurityService,
              private oidcConfigService: OidcConfigService) {
    library.addIconPacks(fas, far);
    oidcConfigService.onConfigurationLoaded.subscribe((configResult: ConfigResult) => {
      // Use the configResult to set the configurations
      const config: OpenIdConfiguration = _.assign(
        configResult.customConfig,
        {
          redirect_url: `${window.location.origin}`,
          silent_renew_url: `${window.location.origin}/silent-renew.html`,
          post_logout_redirect_uri: `${window.location.origin}`,
          log_console_debug_active: false,
          // all other properties you want to set
        }
      );
      logger.debug('oauth2 config:', config);
      oidcSecurityService.setupModule(config, configResult.authWellknownEndpoints);
    });
  }
}
