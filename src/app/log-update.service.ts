import {Injectable} from '@angular/core';
import {SwUpdate, UpdateActivatedEvent, UpdateAvailableEvent} from '@angular/service-worker';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogUpdateService {
  private _updateAvailable: BehaviorSubject<UpdateAvailableEvent>;
  private _updateActivated: BehaviorSubject<UpdateActivatedEvent>;

  constructor(updates: SwUpdate) {
    this._updateAvailable = new BehaviorSubject(null);

    updates.available.subscribe((event: UpdateAvailableEvent) => {
      console.log('current version is', event.current);
      console.log('available version is', event.available);

      this._updateAvailable.next(event);
    });
    updates.activated.subscribe((event: UpdateActivatedEvent) => {
      console.log('old version was', event.previous);
      console.log('new version is', event.current);

      this._updateActivated.next(event);
    });
  }

  get updateAvailable(): Observable<UpdateAvailableEvent> {
    return this._updateAvailable.asObservable();
  }

  get updateActivated(): Observable<UpdateActivatedEvent> {
    return this._updateActivated.asObservable();
  }
}
