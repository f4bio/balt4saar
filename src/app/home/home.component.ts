import {Component, OnInit} from '@angular/core';
import {LogUpdateService} from '../log-update.service';
import {SwUpdate} from '@angular/service-worker';
import {LoggerService} from '@ngx-toolkit/logger';

@Component({
  selector: 'blt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private updates: SwUpdate,
              private logger: LoggerService,
              private logUpdateService: LogUpdateService) {
  }

  ngOnInit(): void {
  }

  get isUpdateAvailable() {
    return this.logUpdateService.updateAvailable.pipe((data) => data);
  }

  doUpdate() {
    this.logger.debug('updating app...');
    this.updates.activateUpdate().then(() => document.location.reload());
  }
}

